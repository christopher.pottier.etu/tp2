import Component from "./Component.js"
export default class Img extends Component{

	constructor(children){
		super('img','src',children)
	}
}