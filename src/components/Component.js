export default class Component{
	tagname;
	attribute;
	children;
	constructor(tagname,attribute,children){
		this.tagname=tagname;
		this.attribute=attribute;
		this.children=children;
		}

	render(){
		if(this.tagname=="img"){
			return `<${this.tagname} ${this.attribute}="${this.children}" />`;
		}
		else if(!this.tagname=='img' && this.children && this.tagname){
			return `<${this.tagname}>${this.children}</${this.tagname}>`;
		}
		else if(Array.isArray(this.children) && this.tagname){
			return `<${this.tagname}>${this.children.join("")}</${this.tagname}>`;
		}
		else if(this.tagname){
			return `<${this.tagname}></${this.tagname}>`;
		}
		else{
			return "";
		}
	}
}